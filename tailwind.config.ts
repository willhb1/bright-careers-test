import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      colors: {
        theme: '#f69044',
        grey: '#e9ecef',
        lightGrey: '#f8fbff',
        blue: '#76a8f4'
      },
    },
  },
  plugins: [],
}
export default config
