type ProgressBarProps = {
    progress: number;  // The progress percentage (0 to 100)
  };
  
  export default function ProgressBar({progress}: ProgressBarProps)  {
    return (
      <div className="relative pt-1 flex">
        <p className="my-auto mr-4">Your Progress - {Math.ceil(progress)}%</p>
        <div className="h-4 flex w-full max-w-[200px] rounded-full my-auto bg-grey">
          <div style={{ width: `${progress}%` }} className="bg-blue rounded-full"></div>
        </div>
      </div>
    );
  };