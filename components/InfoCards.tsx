export default function InfoCards(){
    return(
        <div className="grid lg:grid-cols-3 grid-cols-1 gap-16 xl:gap-24">

            <div className="card info-card">
                <div className="icon-wrapper border-theme bg-[#fff9f5] border-2">
                    <img src="/img/clipboard-question.svg" alt="" />
                </div>
                <div>
                    <h2>24 Questions</h2>
                    <p>Answer 24 questions about your working style and career preferences.</p>
                </div>
            </div>

            <div className="card info-card">
                <div className="icon-wrapper border-[#b9a6ff] bg-[#f9f7ff] border-2">
                    <img src="/img/stopwatch.svg" alt="" />
                </div>
                <div>
                    <h2>2 minutes</h2>
                    <p>Gain insights into your future career in just two minutes</p>
                </div>
            </div>

            <div className="card info-card">
                <div className="icon-wrapper border-[#ffd252] bg-[#fff9f5] border-2">
                    <img src="/img/scissor-cutting.svg" alt="" />
                </div>
                <div>
                    <h2>Personalised advice</h2>
                    <p>Receive personalised advice to guide you on your next steps.</p>
                </div>
            </div>

        </div>
    )
}