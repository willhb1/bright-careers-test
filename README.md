# Built from create-next-app starter TailwindCSS and Typescript
The scope of this prototype is limited by time restrictions. Most noticebly absent are transitions and the ability to scrub/scroll through questions. Core functionality of getting, answering an submitting questions was prioritised aswell as accurate static styles.

Design choices include server-side rendering - this allows a seamless load of the questions with the key downside being the lack of a (component specific) loading state.


# Build and run as standard:
```
yarn dev
yarn build
yarn start
```

Get in touch with contact@willbrown.design for any more info