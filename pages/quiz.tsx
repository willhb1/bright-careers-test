import { useState } from 'react';
import { GetServerSideProps } from 'next';
import { useRouter } from 'next/router';
import axios from 'axios';
import ProgressBar from '../components/ProgressBar';
import InfoCards from '@/components/InfoCards';


type Question = {
  id: string;
  text: string;
};

type Answer = {
  questionId: string;
  answer: number;
};

type QuizProps = {
  questions: Question[];
  latestSubmission: string | null;
};


export default function Quiz( { questions, latestSubmission }: QuizProps ) {
    const [answers, setAnswers] = useState<Answer[]>([]);
    const [currentQuestionIndex, setCurrentQuestionIndex] = useState<number>(0);
    const router = useRouter();

    const handleAnswerChange = (questionId: string, answer: number) => {
      // Remove previous answer for the same question, if exists
      const updatedAnswers = answers.filter(a => a.questionId !== questionId);

      // Add the new answer
      updatedAnswers.push({ questionId, answer });

      setAnswers(updatedAnswers);
    };

    const submitAnswers = async () => {
      try {
        await axios.post(`https://fhc-api.onrender.com/submissions?user=${router.query.user}`, {answers});

        const submissionRes = await axios.get('https://fhc-api.onrender.com/submissions', { params: { user: router.query.user } });
        if (submissionRes.data.latestSubmission) {
          // this will force a rerender and display the completed card
          router.replace(router.asPath); 
        }
      } catch (error: any) {
        console.error("Error submitting answers:", error?.response?.data);
      }
    }


  const progress = ((currentQuestionIndex + 1) / questions.length) * 100;

  return (
    <div className="flex flex-col justify-center items-center">

        <header className='bg-lightGrey px-12 py-8 min-h-[200px] flex flex-col w-full'>
            <div className='container-sm h-full flex flex-col flex-1 w-full mx-auto'>
                <h1 className="text-2xl md:text-4xl font-semibold mt-auto mb-3">
                Career path test
                </h1>
                <p>Discover careers that match your skills and personality</p>
            </div>
        </header>

        <div className="p-12">
            <div className=" container-sm">

                <InfoCards/>

                {
                    latestSubmission ? (
                        <div className="rounded-2xl container-xs mx-auto overflow-hidden mt-12 border border-grey shadow-lg shadow-slate-100 bg-white">
                            <div className="w-full h-[200px] bg-theme">
                              <img className="w-full h-full object-cover" src="/img/graduation.svg" alt="" />
                            </div>
                            <div className="px-8 py-6">
                              <h3 className='font-bold'>Completed on { 
                                  new Date(latestSubmission).toLocaleDateString('en-GB', {
                                      day: 'numeric',
                                      month: 'long',
                                      year: 'numeric'
                                  })
                              }</h3>
                              <p>Well done on completing your test. You can view the results now.</p>
                              <button className="btn-theme px-6 py-3 text-sm mt-4">See your results</button>
                            </div>
                        </div>
                    ) : (

                        <div className="card p-12 mt-12 flex flex-col pt-0">

                            {/* Progress Bar and Question Count */}
                            <div className="mb-6 py-8 border-b border-grey -mx-12 px-12">
                                <ProgressBar progress={progress} />
                            </div>

                            <div className="container-xs mx-auto w-full py-16">
                            {/* Current Question */}
                              <div className='relative'>
                                <p className=" text-theme mt-2 xl:absolute xl:-top-[10px] xl:-left-[70px]">Q{currentQuestionIndex + 1}/{questions.length}</p>
                                <p className="">In a job, I would be motivated by</p>

                                <p className="text-lg mb-12 font-medium">{questions[currentQuestionIndex]?.text}</p>
                              </div>

                              {/* Answer Scale */}
                              <div className="relative mb-6">
                                  {/* Line connecting the buttons */}
                                  
                                  {/* Buttons */}
                                  <div className="flex justify-between relative">
                                      <div className="absolute top-1/2 left-5 right-5 border-t-2 border-blue"></div>
                                  {[...Array(8)].map((_, idx) => (
                                      <button
                                      key={idx}
                                      className='w-10 h-10 rounded-full border-2 border-blue z-10 bg-lightGrey flex'
                                      onClick={() => {
                                          handleAnswerChange(questions[currentQuestionIndex].id, idx);
                                          if (currentQuestionIndex < questions.length - 1) {
                                            setTimeout(() => {
                                              setCurrentQuestionIndex(currentQuestionIndex + 1);                                              
                                            }, 300);
                                          }
                                      }}
                                      >

                                        <div className={`w-5 h-5 rounded-full m-auto transition-all opacity-0 origin-center scale-0 ${answers.find(a => a.questionId === questions[currentQuestionIndex].id)?.answer == idx ? 'bg-blue opacity-100 scale-100':''}`}></div>

                                      </button>
                                  ))}
                                  </div>

                                  {/* Strongly Disagree & Agree Texts */}
                                  <div className="flex justify-between mt-3">
                                    <span>Strongly disagree</span>
                                    <span>Strongly agree</span>
                                  </div>
                              </div>
                            </div>

                            {/* Finish Button */}
                            {answers.length === questions.length && <button onClick={submitAnswers} className="px-12 py-2 btn-theme mx-auto">Finish</button>}
                        </div>
                    )
                }
            </div>
        </div>
    </div>
  );
};


export const getServerSideProps: GetServerSideProps<QuizProps> = async (context) => {
    const user = context.query.user as string;
    let questions = [];
    let latestSubmission = null;

    try{
        const questionsRes = await axios.get('https://fhc-api.onrender.com/questions', { params: { user } });
        questions = questionsRes.data.questions;
        console.log('questions:', questions)
    } catch(error: any){
        console.error("Error fetching questions:", error?.response?.data);
    }
  
    try {
      const submissionRes = await axios.get('https://fhc-api.onrender.com/submissions', { params: { user } });
      latestSubmission = submissionRes.data.latestSubmission;

      console.log('submissionRes:', latestSubmission)
    } catch (error: any) {
      console.error("Error fetching submission:", error?.response?.data);
      // Handle error scenarios, maybe set some default states or show error messages to users
    }
  
    return {
      props: {
        questions,
        latestSubmission
      }
    };
  };

// export default Quiz;