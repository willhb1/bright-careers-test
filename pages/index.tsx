import Link from 'next/link';
import { useState } from 'react';

export default function Home() {
  const [userName, setUserName] = useState('');

  return (
    <div className="p-4 min-h-screen flex w-screen justify-center">
      <div className="m-auto mx-8 max-w-2xl">
        <h1 className="text-2xl md:text-4xl font-semibold mb-4">
          Bright Network - Career Test
        </h1>
        <p className="mb-16 md:text-lg text-md">Built in Next with TailwindCSS & Typescript. Hosted on Vercel. <br/> 
          Enter a username below to navigate to the quiz component. Note that on first time the API needs to start up before you're redirected.
        </p>
        {/* <p className="mb-4">
          Discover careers that match your skills and personality.
        </p> */}
        <div className="flex items-center">
          <input 
            type="text" 
            placeholder="Username" 
            className="flex-grow"
            value={userName}
            onChange={(e) => setUserName(e.target.value)}
          />
          <Link href={userName.length?`/quiz?user=${userName}`:''}>
            <button className="ml-2 btn-theme px-6 py-2">Start</button>
          </Link>
        </div>
      </div>
    </div>
  );
}